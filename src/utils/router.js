import Vue from 'vue';
import Router from 'vue-router';
import store from "../store/_index";

Vue.use(Router);

/**
 * Create a router instance.
 *
 * @param  {Array} routes
 * @return {VueRouter}
 */
export default function router(routes) {
    return new Router({
        routes,
        scrollBehavior,
        mode: 'history',
        saveScrollPosition: false,
        localized: true,
    });
}

/**
 * Add the "authenticated" guard.
 *
 * @param  {Array} routes
 * @return {Array}
 */
export function authGuard(routes) {
    return guard(routes, (to, from, next) => {
        console.log("aba", store.getters['Auth/authenticated']);
        if (localStorage.getItem('token') !== '' &&
            localStorage.getItem('token') !== undefined &&
            localStorage.getItem('token') !== null) {
            next();
        } else {
            next({name: 'auth'})
        }
    })
}

/**
 * Add the "guest" guard.
 *
 * @param  {Array} routes
 * @return {Array}
 */
export function guestGuard(routes) {
    return guard(routes, (to, from, next) => {
        if (store.getters['Auth/authenticated']) {
            next({name: 'home'});
        } else {
            next();
        }
    });
}

//
/**
 * @param  {Array} routes
 * @param  {Function} guard
 * @return {Array}
 */
function guard(routes, guard) {
    routes.forEach(route => {
        if (typeof route.beforeEnter === 'function') {
            route.beforeEnter = function (g1, g2, to, from, next) {
                g1(to, from, next);
                g2(to, from, next);
            }.bind(null, guard, route.beforeEnter);
        } else {
            route.beforeEnter = guard;
        }
    });

    window.routes = routes;

    return routes
}


/**
 * @param  {Route} to
 * @param  {Route} from
 * @param  {Object|undefined} savedPosition
 * @return {Object}
 */
function scrollBehavior(to, from, savedPosition) {
    if (savedPosition) {
        return savedPosition;
    }
    const position = {};
    if (to.hash) {
        position.selector = to.hash;
    }
    if (to.matched.some(m => m.meta.scrollToTop)) {
        position.x = 0;
        position.y = 0;
    }
    return position;
}
