const { version, name } = require('../package.json');

const isProd = process.env.NODE_ENV === 'production';

module.exports = {
  APP_NAME: name,
  APP_VERSION: isProd ? version : 'dev',
};
