import HttpRequest from './../http_request'

class CategoriesProvider extends HttpRequest {
    getCategories() {
        return this.fetch('get-categories');
    }

    getSpecialCategories() {
        return this.fetch('get-special-categories');
    }
}

export default CategoriesProvider
