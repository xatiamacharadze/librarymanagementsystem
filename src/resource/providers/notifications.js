import HttpRequest from './../http_request'

class NotificationsProvider extends HttpRequest {
    markAsRead(notificationId) {
        return this.fetch('mark-as-read/' + notificationId);
    }

    markAsSeen() {
        return this.create('mark-as-seen');
    }

    getNotifications() {
        let params = {
            limit: 10,
            offset: 0
        };
        return this.create('get-notifications', params);
    }
}

export default NotificationsProvider
