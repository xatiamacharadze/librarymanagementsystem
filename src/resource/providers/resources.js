import HttpRequest from './../http_request'

class ResourcesProvider extends HttpRequest {
    getResources() {
        let params = {
            limit: 10,
            offset: 0,
        };
        return this.create('find-resources', params);
    }

    getFilteredResources(filters) {
        return this.create('find-resources', filters);
    }

    quickFindResources(query) {
        let params = {
            limit: 10,
            offset: 0,
            query: query
        };
        return this.create('quick-find-resources', params);
    }

    getResource(bookId) {
        return this.fetch('get-resource/' + bookId);
    }

    getLanguages() {
        return this.fetch('get-languages');
    }


}

export default ResourcesProvider
