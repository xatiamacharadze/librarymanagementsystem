import HttpRequest from './../http_request'

class AuthProvider extends HttpRequest {
    login() {
        return this.fetch('get-authorized-user');
    }

    logout() {
        return this.create('logout');
    }
}

export default AuthProvider
