import HttpRequest from './../http_request'

class UserProvider extends HttpRequest {
    getClient() {
        return this.fetch('get-client');
    }

    getClientFavorites() {
        return this.fetch('get-client-favourite');
    }

    addResourceInFavorites(payload) {
        return this.create('add-favourite', {resourceId: payload.id});
    }

    removeResourceInFavorites(payload) {
        return this.create('remove-favourite', {resourceId: payload.id});
    }

    updateUserProfile(payload) {
        return this.create('update-client', payload);
    }

    getSchools(){
        return this.fetch('get-schools');
    }

    deactivateClientCard(cardId){
        return this.fetch('deactivate-card/' + cardId);
    }

    activateClientCard(cardId){
        return this.fetch('activate-card/' + cardId);
    }

    getClientHistory(payload) {
        return this.create('get-client-resource-copy-borrow-history', payload);
    }
}

export default UserProvider
