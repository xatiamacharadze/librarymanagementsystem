import CategoriesProvider from './providers/categories'
import ResourcesProvider from './providers/resources'
import AuthProvider from './providers/auth'
import UserProvider from './providers/user'
import NotificationsProvider from './providers/notifications'

export const CategoriesService = new CategoriesProvider();
export const ResourcesService = new ResourcesProvider();
export const AuthService = new AuthProvider();
export const UserService = new UserProvider();
export const NotificationsService = new NotificationsProvider();