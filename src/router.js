
import makeRouter from './utils/router';
import {authGuard, guestGuard} from './utils/router'


//Pages
import Home from './components/pages/Home.vue';
import History from './components/pages/History.vue';
import MyWithdrawnBooks from './components/pages/MyWithdrawnBooks.vue';
import Auth from './components/pages/Auth.vue';
import TestAuth from './components/pages/TestAuth.vue';
import Search from './components/pages/Search.vue';
import Book from './components/pages/Book.vue';
import Profile from './components/pages/Profile.vue';
import Category from './components/pages/Category.vue';
import Favorites from './components/pages/Favorites.vue';
import NotFound from './components/pages/NotFound.vue';

//Layouts
import userLayout from './components/layouts/userLayout'

const routes = [
    ...authGuard([
        {
            path: '/',
            component: userLayout,
            children: [
                {path: '/', name: 'home', component: Home, meta: {title: 'Home'}},
                {path: '/history', name: 'history', component: History, meta: {title: 'History'}},
                {path: '/my-readings', name: 'my-readings', component: MyWithdrawnBooks, meta: {title: 'My Readings'}},
                {path: '/search', name: 'search', component: Search, meta: {title: 'Search'}},
                {path: '/favorites', name: 'favorites', component: Favorites, meta: {title: 'Favorites'}},
                {path: '/book/:id', name: 'book', component: Book, meta: {title: 'Book'}},
                {path: '/profile/:id', name: 'profile', component: Profile, meta: {title: 'Profile'}},
                {path: '/category/:name/:id', name: 'category', component: Category, meta: {title: 'Category'}},
            ],
            meta: {
                permission: 'any',
                fail: '/error',
            },
        },
    ]),
    ...guestGuard([
        {path: '/test-auth', name: 'test-auth', component: TestAuth, meta: {title: 'TestAuth'}},
        {path: '/auth', name: 'auth', component: Auth, meta: {title: 'Auth'}},
    ]),
    {path: '*', component: NotFound, meta: {title: 'Not Found'}},
];

const router = makeRouter(routes);

// router.beforeEach((to, from, next) => {
//     document.title = to.meta.title;
//     next();
// });

export default router;
