import Vue from 'vue'
import App from './App.vue'
import './index.scss'
import '../index.html'


import GoogleAuth from 'vue-google-auth'
import * as axios from "axios";

Vue.use(GoogleAuth, { client_id: '1036785699602-p9kb5favo7epsj40l3aho7qbrda49c0s.apps.googleusercontent.com' });
Vue.googleAuth().load();

const token = localStorage.getItem('token');
if (token) {
    axios.defaults.headers.common['Token'] = token
}

import store from './store/_index';
import router from './router';

new Vue({
    el: '#app',
    store,
    router,
    render: h => h(App)
});
