import {ResourcesService, UserService} from '../../resource'

const user = {
    namespaced: true,

    state: {
        userEmptyObj: {
            active: false,
            email: "",
            firstName: "",
            id: -1,
            imageUrl: null,
            imgUrl: null,
            lastName: "",
            phone: "",
            school: {
                description: "",
                id: -1,
                name: "",
                university: "",
            },
        },
        user: null,
        userFavorites: [],
        withdrawnBooks: [],
        history: [],
        schools: [],
    },

    mutations: {
        SET_CLIENT(state, user) {
            state.user = user;
        },

        SET_CLIENT_FAVORITES(state, userFavorites) {
            state.userFavorites = userFavorites;
        },

        SET_WITHDRAWN_BOOKS(state, withdrawnBooks) {
            state.withdrawnBooks = withdrawnBooks;
        },

        SET_HISTORY(state, history) {
            state.history = history;
        },

        SET_SCHOOLS(state, schools) {
            state.schools = schools;
        },

        SET_CARD_ACTIVE(state, info) {
            state.user.cards[info.id].active = info.value;
        },

        ADD_TO_FAVOURITES(state, resource) {
            let filteredResource = state.userFavorites.filter(item => item.id === resource.id);
            if (!filteredResource.length)
                state.userFavorites.push(resource);
        }
    },

    actions: {
        getClient({commit}) {
            return UserService.getClient().then(response => {
                return commit('SET_CLIENT', response.data);
            });
        },

        setClient({commit}, client) {
            commit('SET_CLIENT', client);
        },

        getClientFavorites({commit}) {
            return UserService.getClientFavorites().then(response => {
                return commit('SET_CLIENT_FAVORITES', response.data);
            });
        },

        getClientWithdrawnBooks({commit}, filters) {
            return new Promise((resolve, reject) => {
                return  UserService.getClientHistory(filters).then(response => {
                    commit('SET_WITHDRAWN_BOOKS', response.data.resultList);
                    resolve(response.data);
                }).catch(error => {
                    reject(error);
                });
            });
        },

        getClientHistory({commit}, filters) {
            return new Promise((resolve, reject) => {
                return UserService.getClientHistory(filters).then(response => {
                    commit('SET_HISTORY', response.data.resultList);
                    resolve(response.data);
                }).catch(error => {
                    reject(error);
                });
            });
        },

        addResourceInFavorites({commit}, payload) {
            return UserService.addResourceInFavorites(payload).then(() => {
                return commit('ADD_TO_FAVOURITES', payload);
            });
        },

        removeResourceInFavorites({commit}, payload) {
            return UserService.removeResourceInFavorites(payload);
        },

        updateUserProfile({commit}, payload) {
            return UserService.updateUserProfile(payload).then(response => {
                commit('SET_CLIENT', response.data);
            });
        },

        getSchools({commit}) {
            return UserService.getSchools().then(response => {
                commit('SET_SCHOOLS', response.data.resultList);
            });
        },

        deactivateClientCard({commit}, info) {
            commit('SET_CARD_ACTIVE', {value: false, id: info.i});
            return UserService.deactivateClientCard(info.id).then(response => {
            });
        },

        activateClientCard({commit}, info) {
            commit('SET_CARD_ACTIVE', {value: true, id: info.i});
            return UserService.activateClientCard(info.id).then(response => {
            });
        },
    },

    getters: {
        user: state => state.user ? state.user : state.userEmptyObj,
        userFavorites: state => state.userFavorites,
        withdrawnBooks: state => state.withdrawnBooks,
        history: state => state.history,
        schools: state => state.schools,
    }
};

export default user