import {AuthService} from '../../resource'
import * as axios from "axios";

const auth = {
    namespaced: true,

    state: {
        status: '',
    },

    mutations: {

        SET_TOKEN(state, token) {
            if (token === '') localStorage.removeItem('token');
            else localStorage.setItem('token', token);
        },

        AUTH_REQUEST: (state) => {
            state.status = 'loading'
        },

        AUTH_SUCCESS: (state, token) => {
            state.status = 'success';
            state.token = token
        },

        AUTH_ERROR: (state) => {
            state.status = 'error'
        },

    },

    actions: {
        login({commit}, token) {
            return new Promise((resolve, reject) => {
                commit('AUTH_REQUEST');
                if (!token) token = localStorage.getItem('token');
                axios.defaults.headers.common['Token'] = token;
                AuthService.login()
                    .then(resp => {
                        console.log(resp.success === true);
                        if (resp.success) {
                            commit('SET_TOKEN', token);
                            // Add the following line:
                            commit('AUTH_SUCCESS');
                            resolve(resp.data);
                        } else {
                            commit('AUTH_ERROR');
                            commit('SET_TOKEN', '');
                            reject('error');
                        }
                    })
                    .catch(err => {
                        commit('AUTH_ERROR');
                        commit('SET_TOKEN', '');
                        reject(err)
                    })
            });
        },

        logout({commit}) {
            console.log('wavedii');
            return new Promise((resolve, reject) => {
                // remove the axios default header
                delete axios.defaults.headers.common['Token'];
                AuthService.logout()
                    .then(resp => {
                        commit('SET_TOKEN', '');
                        window.location.reload();
                        resolve();
                    })
                    .catch(err => {
                        window.location.reload();
                        reject(err);
                    })
            });
        },
    },

    getters: {
        authenticated: () => {
            console.log('tokenatori', localStorage.getItem('token'));
            return localStorage.getItem('token') !== '' &&
                localStorage.getItem('token') !== undefined &&
                localStorage.getItem('token') !== null
        },
        authStatus: state => state.status,
    }
};

export default auth