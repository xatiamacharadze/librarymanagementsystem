import {ResourcesService} from '../../resource'


class Filters {

    constructor(filters) {
        this.init();
        if (filters)
            this.setFilters(filters);
    }

    init() {
        this.filters = {
            author: null,
            categoryIds: null,
            fromEditionDate: null,
            id: null,
            limit: 10,
            offset: 0,
            name: null,
            publisher: null,
            resourceType: null,
            languageId: null,
            toEditionDate: null
        }
    }

    getFilters() {
        return this.filters;
    }

    setFilters(filters) {
        for (let field in filters) {
            this.filters[field] = filters[field];
        }
    }
}

const resources = {
    namespaced: true,

    state: {
        resources: [],
        resource: [],
        detailedMessage: "",
        languages: [],
        filters: {
            author: null,
            categoryIds: null,
            fromEditionDate: null,
            id: null,
            limit: 10,
            offset: 0,
            name: null,
            publisher: null,
            resourceType: null,
            languageId: null,
            toEditionDate: null
        },
    },

    mutations: {
        GET_RESOURCES(state, resources) {
            state.resources = resources;
        },

        GET_RESOURCE(state, resource) {
            state.resource = resource;
        },

        SET_DETAILED_MESSAGE(state, message) {
            state.detailedMessage = message;
        },

        SET_FILTER_VALUE(state, filterValue) {
            state.filters[filterValue.name] = filterValue.value;
        },

        SET_LANGUAGES(state, languages) {
            state.languages = languages;
        },

        SET_FILTER(state, filters) {
            state.filters = filters
        }
    },

    actions: {
        getResources({commit}) {
            return ResourcesService.getResources().then(response => {
                return commit('GET_RESOURCES', response.data);
            });
        },

        getResource({commit}, bookId) {
            return ResourcesService.getResource(bookId).then(response => {
                return commit('GET_RESOURCE', response.data);
            });
        },

        getFilteredResources({state}, filters = null) {

            let filterData = null;
            if (filters)
                filterData = new Filters(filters).getFilters();
            else
                filterData = new Filters(state.filters).getFilters();

            return new Promise((resolve, reject) => {
                return ResourcesService.getFilteredResources(filterData).then(response => {
                    resolve(response.data)
                }).catch(error => {
                    console.log('error')
                    // return reject(error.response.data.message);
                });
            });
        },

        getLanguages({commit}) {
            return ResourcesService.getLanguages().then(response => {
                return commit('SET_LANGUAGES', response.data.resultList);
            });
        },

        quickFindResources({}, detailedMessage) {
            return new Promise((resolve, reject) => {
                return ResourcesService.quickFindResources(detailedMessage).then(response => {
                    resolve(response.data)
                }).catch(error => {
                    // return reject(error.response.data.message);
                });
            });
        },

        setDetailedMessage({commit}, message) {
            commit('SET_DETAILED_MESSAGE', message);
        },

        setFilterValue({commit}, filterValue) {
            commit('SET_FILTER_VALUE', filterValue);
        },

        clearFilters({commit}) {
            commit('SET_FILTER', {
                author: null,
                categoryIds: null,
                fromEditionDate: null,
                id: null,
                limit: 10,
                offset: 0,
                name: null,
                publisher: null,
                resourceType: null,
                languageId: null,
                toEditionDate: null
            });
        }
    },

    getters: {
        resources: state => state.resources,
        resource: state => state.resource,
        filters: state => state.filters,
        detailedMessage: state => state.detailedMessage,
        languages: state => state.languages,
    }
};

export default resources