import {CategoriesService} from '../../resource'

const categories = {
    namespaced: true,

    state: {
        categories: [],
        specialCategories: [],
    },

    mutations: {
        GET_CATEGORIES(state, response) {
            state.categories = response.data.resultList;
        },

        GET_SPECIAL_CATEGORIES(state, response) {
            state.specialCategories = response.data.resultList;
        }
    },

    actions: {
        getCategories({commit}) {
            return CategoriesService.getCategories().then(response => {
                return commit('GET_CATEGORIES', response);
            });
        },

        getSpecialCategories({commit}) {
            return CategoriesService.getSpecialCategories().then(response => {
                return commit('GET_SPECIAL_CATEGORIES', response);
            });
        },
    },

    getters: {
        categories: state => state.categories,
        specialCategories: state => state.specialCategories,
    }
};

export default categories