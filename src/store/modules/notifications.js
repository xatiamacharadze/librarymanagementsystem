import {AuthService, NotificationsService} from '../../resource'
import * as axios from "axios";

const auth = {
    namespaced: true,

    state: {
        notifications: [],
        newNotificationCount: 0
    },

    mutations: {

        SET_NOTIFICATION_READ(state, notificationId) {
            for (let i = 0; i < state.notifications.length; i++) {
                if (state.notifications[i].id === notificationId) {
                    state.notifications[i].read = true;
                    break
                }
            }
        },

        SET_NOTIFICATIONS_SEEN(state) {
            state.newNotificationCount = 0
        },

        SET_NOTIFICATIONS(state, notifications) {
            state.notifications = notifications;
        },

        SET_NEW_NOTIFICATION_COUNT(state, count) {
            state.newNotificationCount = count;
        }

    },

    actions: {
        getNotifications({commit}) {
            return new Promise((resolve, reject) => {
                NotificationsService.getNotifications()
                    .then(resp => {
                        if (resp.success) {
                            commit('SET_NOTIFICATIONS', resp.data.notifications.resultList);
                            commit('SET_NEW_NOTIFICATION_COUNT', resp.data.count);
                            resolve(resp)
                        } else {
                            reject(resp)
                        }
                    })
                    .catch(err => {
                        reject(err)
                    })
            });
        },

        markAsRead({commit}, notificationId) {
            return new Promise((resolve, reject) => {
                NotificationsService.markAsRead(notificationId)
                    .then(resp => {
                        if (resp.success) {
                            commit('SET_NOTIFICATION_READ', notificationId);
                            resolve(resp)
                        } else {
                            reject(resp)
                        }
                    })
                    .catch(err => {
                        reject(err)
                    })
            });
        },

        markAsSeen({commit}) {
            return new Promise((resolve, reject) => {
                NotificationsService.markAsSeen()
                    .then(resp => {
                        if (resp.success) {
                            commit('SET_NOTIFICATIONS_SEEN');
                            resolve(resp)
                        } else {
                            reject(resp)
                        }
                    })
                    .catch(err => {
                        reject(err)
                    })
            });
        },
    },

    getters: {
        notifications: state => state.notifications,
        newNotificationCount: state => state.newNotificationCount,
    }
};

export default auth