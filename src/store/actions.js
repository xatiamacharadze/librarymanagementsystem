export default {
    setLayout (context, layout) {
        context.commit('setLayout', layout);
    }
};
