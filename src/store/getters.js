export default {
    layout: state => state.layout,
    books: state => state.books,
};
